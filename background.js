chrome.app.runtime.onLaunched.addListener(function() {
  var options = {
    'id': 'Chrome Kiosk App',
    'state': 'fullscreen'
  };
  chrome.app.window.create('application.html', options);

  // Keep Chromebit alive
  chrome.power.requestKeepAwake('display');
});