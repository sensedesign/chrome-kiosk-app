(function($) {
  window.onload = function() {
    var manifest = chrome.runtime.getManifest();
    $('.info').text(manifest.name + ': Version ' + manifest.version);
  }
})(jQuery);