# Chrome Kiosk App

The "Chrome Kiosk App" is a basic implementation for showing a website on the screen.

## Needed changes in files

  1. Configure manifest.json
  2. Change "id" in background.js
  3. Change "HTML title" and "webview src" in application.html
  4. Change layout and design in app/application.css (if needed)
  
## Test and upload app to chrome app store

A description how to upload a Chrome App can be found here:
https://support.google.com/chrome/a/answer/2714278?hl=en

## Prepare Chromebit for Kiosk mode

Follow all the instructions, even wiping the Chromebit before you start
http://www.novisign.com/chrome/how-to-put-non-managed-chromebox-in-kiosk-mode/